from tkinter import *
from PIL import Image, ImageTk

root = Tk()

mapFrame=Frame(root)
mapFrame.place(relx=0, rely=0, relheight=0.85, relwidth=1)
subMapFrame = Frame(mapFrame)
subMapFrame.place(relx=0.05,rely=0.1,relwidth=0.9, relheight=0.9)

image = Image.open("images/test.png")
#img_copy= image.copy()
background_image = ImageTk.PhotoImage(image)
background = Label(subMapFrame, image=background_image)
background.pack()

root.mainloop()

input('Press any key to exit')