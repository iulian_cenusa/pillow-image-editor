from tkinter import *
from PIL import ImageTk, Image, ImageFilter
from tkinter import filedialog # for working with files / open files

# --------------------
# initializam fereastra
fereastra = Tk()
fereastra.title("Python Image Filter by Iulian Cenusa")
# Icon page: http://www.iconarchive.com/show/plex-android-icons-by-cornmanthe3rd/photo-editor-icon.html
fereastra.iconbitmap('title.ico')
# --------------------
fereastra.filename = filedialog.askopenfilename(initialdir='/GIT/pillow/',title="Test",filetypes=(("png files",'*.png'),("all files","*.*")) )

img = ImageTk.PhotoImage(Image.open(fereastra.filename))

label_img = Label(fereastra, image=img).pack()
# --------------------
fereastra.mainloop()