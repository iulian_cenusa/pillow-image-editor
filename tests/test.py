#Autor: Iulian Cenusa
#
from PIL import Image , ImageFilter
#------------------
#test1 - load image
try:
    im = Image.open("images/test.png")
except:
    print "Unable to load image"
#------------------
#test2 - image informations
print "The size of the Image is: "
print(im.format, im.size, im.mode)
#------------------
#test3 - blur image
im2o = Image.open("images/test.png")
#------------------
bl = im2o.filter(ImageFilter.BLUR)
#------------------
im2o.show()
bl.show()
#------------------
#test4 - show image as custom thumbnail
im3 = Image.open("images/test.png")
size = (128, 128)
im3.thumbnail(size)
im3.show()
#------------------
#test4 - filters
#CONTOUR
#im4 = Image.open("test.png")
#im4 = im4.filter(ImageFilter.CONTOUR)
#im4.show()
#List of other filters
#BLUR
#CONTOUR
#DETAIL
#EDGE_ENHANCE
#EDGE_ENHANCE_MORE
#EMBOSS
#FIND_EDGES
#SMOOTH
#SMOOTH_MORE
#SHARPEN
im5 = Image.open("images/test.png")
im5 = im5.filter(ImageFilter.SHARPEN)
im5.show()
#------------------
#test5 - save as other format
im6 = Image.open("images/test.png")
im6 = im6.filter(ImageFilter.CONTOUR)
size = (500, 350)
im6.thumbnail(size)
im6.show()
im6.save('contour.png','png')
#------------------
#test6 - convert to B&W
im7 = Image.open("images/test.png")
im7g = im7.convert('L')
size = (600, 400)
im7g.thumbnail(size)
im7g.show()
im7g.save('gray.png','png')
#------------------
#test7 - rotate & save
im8 = Image.open("images/test.png")
im8r = im8.rotate(45)
size = (880, 755)
im8r.thumbnail(size)
im8r.show()
im8r.save('rotate.png','png')


