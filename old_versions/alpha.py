# autor: iulian cenusa
# 2 dec 2019
# GUI program template with tkinter - version 2
# --------------------
from tkinter import *  # Tkintek lib
from PIL import Image, ImageFilter, ImageTk  # Pillow lib


# --------------------
# global variable definitions
#
#
#
# --------------------
# functions definitions
#
# test functions+exit
def test():
    pass


# def test_e():
#    exit(0)
# not used anymore as exit function
# --------------------
# pillow functions
def img_open():
    pass


def blur():
    blur_img = ImageTk.PhotoImage(Image.open("images/test.png").filter(ImageFilter.BLUR))
    background = Label(work_frame, image=blur_img)
    background.pack()


def contour():
    cnt_img = ImageTk.PhotoImage(Image.open("images/test.png").filter(ImageFilter.CONTOUR))
    background = Label(work_frame, image=cnt_img)
    background.pack()


def detail():
    det_img = ImageTk.PhotoImage(Image.open("images/test.png").filter(ImageFilter.DETAIL))
    background = Label(work_frame, image=det_img)
    background.pack()


# --------------------
# initializam fereastra
fereastra = Tk()
fereastra.title("Python Image Filter by Iulian Cenusa")
# Icon page: http://www.iconarchive.com/show/plex-android-icons-by-cornmanthe3rd/photo-editor-icon.html
fereastra.iconbitmap('title.ico')
# --------------------
# toolbar
toolB = Frame(fereastra)
new_btn = Button(toolB, text="New", command=test)
open_btn = Button(toolB, text="Open", command=img_open)
# --------------------
blur_btn = Button(toolB, text="Blur", command=blur)
contour_btn = Button(toolB, text="Contour", command=contour)
detail_btn = Button(toolB, text="Detail", command=detail)
# --------------------
exit_btn = Button(toolB, text="Exit", command=fereastra.quit)
# --------------------
new_btn.pack(side=LEFT, padx=2, pady=2)
open_btn.pack(side=LEFT, padx=2, pady=2)
blur_btn.pack(side=LEFT, padx=2, pady=2)
contour_btn.pack(side=LEFT, padx=2, pady=2)
detail_btn.pack(side=LEFT, padx=2, pady=2)
# --------------------
exit_btn.pack(side=LEFT, padx=2, pady=2)
# --------------------
toolB.pack(side=TOP, fill=X)
# --------------------
# workzone
work_frame = Frame(fereastra, width=800, height=600)  # main zone
background_image = ImageTk.PhotoImage(Image.open("images/test.png"))
# tests - passed
# background_image = ImageTk.PhotoImage(Image.open("test.png").filter(ImageFilter.BLUR))
# background_image = ImageTk.PhotoImage(Image.open("test.png").filter(ImageFilter.CONTOUR))
# background_image = ImageTk.PhotoImage(Image.open("test.png").filter(ImageFilter.DETAIL))
background = Label(work_frame, image=background_image)
background.pack()
info = Label(work_frame, text="Image properties: " + str(
    (Image.open("images/test.png").format, Image.open("images/test.png").size, Image.open("images/test.png").mode)))
# info = Label(work_frame, text="Image properties: " + str((Image.open("test.png").format )))
info.pack()
work_frame.pack()
# --------------------
# statusBar
version = " Alpha v0.1 "
status_bar = Label(fereastra, text="@IulianCenusa, December 2019" + version, bd=1, relief=SUNKEN, anchor=W)
# status bar with border
status_bar.pack(side=BOTTOM, fill=X)
# --------------------
# main loop
fereastra.mainloop()
# --------------------
