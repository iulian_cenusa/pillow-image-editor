from tkinter import *
from PIL import ImageTk, Image, ImageFilter
from tkinter import filedialog # for working with files / open files
# --------------------
def blur():
    blur_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.BLUR))
    label_img.configure(image=blur_img).grid(row=1, column=0, columnspan=4)

def contour():
    cnt_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.CONTOUR))
    label_img.configure(image=cnt_img).grid(row=1, column=0, columnspan=4)

def detail():
    det_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.DETAIL))
    label_img.configure(image=det_img).grid(row=1, column=0, columnspan=4)

def def_img():
    label_img.configure(image=img)
def open_img():
    global img
    fereastra.filename = filedialog.askopenfilename(initialdir='/GIT/pillow/', title="Test",
                                                    filetypes=(("png files", '*.png'), ("all files", "*.*")))
    img = ImageTk.PhotoImage(Image.open(fereastra.filename))
    label_img.configure(image=img).grid(row=1, column=0, columnspan=4)
# --------------------
# initializam fereastra
fereastra = Tk()
fereastra.title("Python Image Filter by Iulian Cenusa")
# Icon page: http://www.iconarchive.com/show/plex-android-icons-by-cornmanthe3rd/photo-editor-icon.html
fereastra.iconbitmap('../title.ico')
# --------------------
# lista imagini
#img = ImageTk.PhotoImage(Image.open("test.png"))
#det_img = ImageTk.PhotoImage(Image.open("test.png").filter(ImageFilter.DETAIL))
#cnt_img = ImageTk.PhotoImage(Image.open("test.png").filter(ImageFilter.CONTOUR))
#blur_img = ImageTk.PhotoImage(Image.open("test.png").filter(ImageFilter.BLUR))
# --------------------
#Meniu frame
meniu = LabelFrame(fereastra, text="Bara de meniu")
open_btn = Button(meniu, text="Open", command=open_img)
def_btn = Button(meniu, text="Default", command=def_img)
blur_btn = Button(meniu, text="Blur", command=blur)
contour_btn = Button(meniu, text="Contour", command=contour)
detail_btn = Button(meniu, text="Detail", command=detail)
exit_btn = Button(meniu, text="Exit", command=fereastra.quit)
# --------------------
open_btn.grid(row=0, column=0)
def_btn.grid(row=0, column=1)
blur_btn.grid(row=0, column=2)
contour_btn.grid(row=0, column=3)
detail_btn.grid(row=0, column=4)
exit_btn.grid(row=0, column=5)
# --------------------
#Workzone frame
workzone = LabelFrame(fereastra, text="Zona de lucru")
label_img = Label(workzone)
label_img.grid(row=1, column=0, columnspan=4)
status = Label(workzone, text="@IulianCenusa " + " December2019 " + " v 0.5 ", bd=1, relief=SUNKEN)
status.grid(row=2, column=0, columnspan=4, sticky=W + E)
# --------------------
meniu.pack()
workzone.pack()
# --------------------
fereastra.mainloop()
