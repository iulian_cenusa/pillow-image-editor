'''
Autor: Iulian Cenusa
Date: dec2019 - jan2020
Image manipulation program
Open,apply filters, rotate and save images
'''
from tkinter import *
from PIL import ImageTk, Image, ImageFilter
from tkinter import filedialog  # for working with files / open files
from tkinter import messagebox #to be used in message boxes
import sys
# --------------------
version = ' v0.9 ' #version variable - used to print version number in footer
info_text = '[RO]                   [EN]\n' \
            'OPEN - deschide o iamgine\n' \
            'SAVEl' \
            '' \
            '' \
            '' \
            '' \
            '' \
            '' \
            '' \
            '' \
            '' \
            ''
#variable used as text in help messagebox
# --------------------
# FUNCTIONS
# --------------------
def exit_w():
    sys.exit(0)
def rotate(  nr ):
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).rotate(nr))
    tmp = Image.open(fereastra.filename).rotate(nr)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def sharp():
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.SHARPEN))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.SHARPEN)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def smm():
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.SMOOTH_MORE))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.SMOOTH_MORE)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def sm():
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.SMOOTH))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.SMOOTH)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def edges():
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.FIND_EDGES))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.FIND_EDGES)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def emb():
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.EMBOSS))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.EMBOSS)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def edge_em():
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.EDGE_ENHANCE_MORE))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.EDGE_ENHANCE_MORE)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def edge_e():
    new_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.EDGE_ENHANCE))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.EDGE_ENHANCE)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=new_img).grid(row=1, column=0, columnspan=4)
def blur():
    blur_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.BLUR))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.BLUR)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=blur_img).grid(row=1, column=0, columnspan=4)
def contour():
    cnt_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.CONTOUR))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.CONTOUR)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=cnt_img).grid(row=1, column=0, columnspan=4)
def detail():
    det_img = ImageTk.PhotoImage(Image.open(fereastra.filename).filter(ImageFilter.DETAIL))
    tmp = Image.open(fereastra.filename).filter(ImageFilter.DETAIL)
    tmp.save('tmp.png','png')
    fereastra.filename = 'tmp.png'
    label_img.configure(image=det_img).grid(row=1, column=0, columnspan=4)
def def_img():
    label_img.configure(image=img)
    tmp = Image.open(fereastra.filename_bkp)
    tmp.save('tmp.png', 'png')
def open_img():
    global img
    global img_tmp
    fereastra.filename = filedialog.askopenfilename(initialdir='/GIT/pillow/images/', title="Open a image",filetypes=(("Png files", '*.png'),("JPEG files", '*.jpg'), ("All files", "*.*")))
    fereastra.filename_bkp = fereastra.filename
    img = ImageTk.PhotoImage(Image.open(fereastra.filename))
    img_tmp = Image.open(fereastra.filename)
    label_img.configure(image=img).grid(row=1, column=0, columnspan=4)
def save_img():
    fereastra.filename = filedialog.asksaveasfilename(filetypes=[("Png files", '.png'),("JPEG files", '.jpg'),('All files', '.*')],defaultextension='.png')
    tmp = Image.open('tmp.png')
    tmp.save(fereastra.filename, fereastra.filename[-3:])
def info():
    messagebox.showinfo("Help" , info_text)
# --------------------
# initializam fereastra
fereastra = Tk()
fereastra.title("Python Image Filter by Iulian Cenusa")
# Icon page: http://www.iconarchive.com/show/plex-android-icons-by-cornmanthe3rd/photo-editor-icon.html
#fereastra.iconbitmap('title.ico')
# --------------------
#Meniu frame
meniu = LabelFrame(fereastra, text="Bara de meniu")
#Buttons - General
open_btn = Button(meniu, text="Open", command=open_img)
save_btn = Button(meniu, text="Save", command=save_img)
help_btn = Button(meniu, text="Help", command=info)
exit_btn = Button(meniu, text="Exit", command=exit_w)
#Buttons - Effects
def_btn = Button(meniu, text="Default", command=def_img)
blur_btn = Button(meniu, text="Blur", command=blur)
contour_btn = Button(meniu, text="Contour", command=contour)
detail_btn = Button(meniu, text="Detail", command=detail)
egde_btn = Button(meniu, text="Edge Enhance", command=edge_e)
egdem_btn = Button(meniu, text="Edge Enhance More", command=edge_em)
embr_btn = Button(meniu, text="Embross", command=emb)
edge_btn = Button(meniu, text="Find Edges", command=edges)
smooth_btn = Button(meniu, text="Smooth", command=sm)
smoothm_btn = Button(meniu, text="Smooth More", command=smm)
sharp_btn = Button(meniu, text="Sharpen", command=sharp)
#Rotate frame + buttons
rot = LabelFrame(fereastra, text="Rotate")
scala = Scale(rot,from_=0, to =360, orient = HORIZONTAL) #Scale for setting rotation angle
rot_btn = Button(rot, text="Rotate", command=lambda : rotate(scala.get()))
#Grid Placement
open_btn.grid(row=0, column=0)
save_btn.grid(row=0, column=1)
def_btn.grid(row=0, column=2)
help_btn.grid(row=0, column=3)
exit_btn.grid(row=0, column=4)
#-----------------------------
blur_btn.grid(row=1, column=0)
contour_btn.grid(row=1, column=1)
detail_btn.grid(row=1, column=2)
egde_btn.grid(row=1, column=3)
egdem_btn.grid(row=1, column=4)
embr_btn.grid(row=1, column=5)
edge_btn.grid(row=1, column=6)
smooth_btn.grid(row=1, column=7)
smoothm_btn.grid(row=1, column=8)
sharp_btn.grid(row=1, column=9)
#-----------------------------
scala.grid(row=2,column=0)
rot_btn.grid(row=2, column=1)
#Workzone frame
workzone = LabelFrame(fereastra, text="Zona de lucru")
label_img = Label(workzone)
label_img.grid(row=1, column=0, columnspan=4)
status = Label(workzone, text="@IulianCenusa " + " Jan2020 " + version, bd=1, relief=SUNKEN)
status.grid(row=2, column=0, columnspan=4, sticky=W + E)
# --------------------
meniu.pack()
rot.pack()
workzone.pack()
# --------------------
fereastra.mainloop()
