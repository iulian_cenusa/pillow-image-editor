from cx_Freeze import setup, Executable

base = None

executables = [Executable("app.py", base=base)]

packages = ["idna","Tkinter","PIL"]
options = {
    'build_exe': {
        'packages':packages,
    },
}

setup(
    name = "test",
    options = options,
    version = "0.9",
    description = 'Desc',
    executables = executables
)